#!/usr/local/env/python

# Author: Adrian Elinow <adrian@codedevl.com>

import sys
import requests
import json
from pytube import YouTube as YT
from datetime import date, time, datetime, timedelta

DEBUG = False

def load_json_file(filename):
    ''' Simply meant to read out json file into python dictionary format '''
    raw = open(filename).read()
    data = json.loads(raw)
    return data

def get_recent_upload_data(api_key, channel_config):
    ''' given a specific channel (id), and download ruleset,
    will retrieve links of recently uploaded videos from channel

    args:
    Google API key
    Config dictionary for channel
    '''
    base_search_url = 'https://www.googleapis.com/youtube/v3/search?'
    
    links = []
    url = "https://www.googleapis.com/youtube/v3/search?key={0}&channelId={1}&part=snippet,id&order=date&maxResults=25".format(api_key, channel_config['id'])
    try:
        query = requests.get(url)
        reply = json.loads( (query.content).decode('utf-8') )
    except:
        if DEBUG:
            print('ERROR; Unsuccessful Query/Reply for "{0}"'.format(channel_config['name']))
    if DEBUG:
        print('Successful Query and Reply for "{0}"'.format(channel_config['name']))
    return reply 

def select_valid_videos(query_reply, channel_config, last_updated):
    ''' extracts valid videos from query_reply
    valid videos selected based on config file
    '''
    prev_update_timestamp = datetime.strptime(last_updated, "%Y-%m-%d %H:%M:%S")
    
    videos = []
    for v in query_reply['items']:
        try:
            if v['id']['kind'] == "youtube#video":
                videos.append( v )
        except:
            if DEBUG:
                print('Found non-video resource:\n\t{0}'.format(v['id']['kind']))
    valids = []
    for v in videos:
        publish_timestamp = datetime.strptime(v['snippet']['publishedAt'][:-5], "%Y-%m-%dT%H:%M:%S")
        video_title = v['snippet']['title']
        if DEBUG:
            print("Now processing: '{0}'".format(video_title))
        
        if prev_update_timestamp - publish_timestamp < timedelta(days=0):
            for re in channel_config['ruleset']['regex']:
                if re in video_title: #This section may require additionl functionality
                    if DEBUG:
                        print("'{0}' is valid".format(video_title))
                    valids.append(v)
                else:
                    if DEBUG:
                        print("\t'{0}' != '{1}'".format(video_title, re))
        else:
            if DEBUG:
                print('\tpublish {0}\n\tprev {1}\n\tdelta {2}'.format(publish_timestamp, prev_update_timestamp, publish_timestamp - prev_update_timestamp))
        # Check that publish timestamp is after most recent update timestamp
        # Check that video title meets requirements (if any)
        # >>> If video metadata meets requirements, append the id to 'validated'
                
    print('\nFound {0} valid videos for "{1}"'.format(len(valids), channel_config['name']))
    return valids
          
def download_videos_by_links(links, folder_path):
    for l in links:
        try:
            yt = YT(l)
            if DEBUG:
                print('\tDownloading: "{0}" to {1}'.format(yt.title, folder_path))
            stream = yt.streams.first()
            stream.download(folder_path)
        except Exception as e:
            print("Unexpected Error: \n{0}".format(e))

def main():
    if len(sys.argv) is not 2:
        print('Error; program requires a \'JSON\' configuration file as argument')
    config = load_json_file(sys.argv[1])
    DEBUG = config['debug']
    
    for channel in config['channels']:
        print("Processing Channel: {0}".format(channel['name']))

        data = get_recent_upload_data(config['api_key'], channel)
        
        valid_videos = select_valid_videos(data, channel, config['last_updated'])

        valid_links = []
        for vv in valid_videos:
            if DEBUG:
                print("\t'{0}' by {1}, published at {2}".format(vv['snippet']['title'], vv['snippet']['channelTitle'], vv['snippet']['publishedAt']))
            valid_links.append('https://www.youtube.com/watch?v={0}'.format(vv['id']['videoId']))

        if ( not config['allow_auto_save'] ) and \
           ( 'y' not in input('Proceed with {0} downloads for "{1}"? (y/n)\n\t>>>'.format(len(valid_links), channel['name'])) ):
            break
        
        if DEBUG:
            print('Starting download sequence')
            
        if channel['save_folder_path']:
            download_videos_by_links( valid_links, channel['save_folder_path'] )
        else:
            download_videos_by_links( valid_links, config['default_save_folder_path'] )
        # Resets list
        valid_videos = []
        if DEBUG:
            print("Finished processing for channel: {0}".format(channel['name']))

    print("Update Finished")

if __name__ == '__main__':
    main()
